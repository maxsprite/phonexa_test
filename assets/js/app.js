/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single scss file (app.scss in this case)
require('../scss/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

function createTransaction(data, requestType, token)
{
   let formData = new FormData();
   formData.append('query', data);

   $.ajax({
      url: '/api/transaction/create?type=' + requestType,
      method: 'POST',
      contentType: 'application/' + requestType,
      processData: false,
      data: data,
      beforeSend: function(xhr) {
         xhr.setRequestHeader('Authorization', 'Bearer ' + token);
      },
      success: function (response) {
         let isXml = $.isXMLDoc(response);
         if (isXml) {
            $('#result-response').val((new XMLSerializer()).serializeToString(response));
         } else {
            $('#result-response').val(JSON.stringify(response));
         }
      },
      error: function (xhr, type, message) {
         $('#result-response').val(xhr.status + ': ' + message);
      }
   });
}

$(document).ready(function () {

   $('#jwt-token-form').submit(function (event) {
      event.preventDefault();

      let url = $(this).attr('action');
      let data = $(this).serializeArray();

      let formData = new FormData();
      $.each(data, function (key, item) {
         formData.append(item.name, item.value);
      });

      $('.get-over-here').children('.spinner').toggleClass('d-none');
      $('.get-over-here').addClass('disabled');
      $('.get-over-here').attr('disabled', true);

      $.ajax({
         url: url,
         method: 'POST',
         data: formData,
         processData: false,
         contentType: false,
         success: function (response) {
            $('.get-over-here').children('.spinner').toggleClass('d-none');

            let json = JSON.parse(response);
            $('#token').val(json.token);
            $('#result-response').val(response);
         }
      });
   });

   $('#transaction-form').submit(function (event) {
      event.preventDefault();

      const requestType = $('input[name=requestType]:checked').val();
      const url = $(this).attr('action') + '?type=' + requestType;

      let data = $(this).serializeArray();

      let formData = new FormData();
      $.each(data, function (key, item) {
         formData.append(item.name, item.value);
      });

      $('.subziro').children('.spinner').toggleClass('d-none');
      $.ajax({
         url: url,
         method: 'POST',
         data: formData,
         processData: false,
         contentType: false,
         success: function (response) {
            $('.subziro').children('.spinner').toggleClass('d-none');

            let isXml = $.isXMLDoc(response);
            let data = response;
            let token = $('#token').val();

            if (isXml) {
               $('#result-response').val((new XMLSerializer()).serializeToString(data));
               setTimeout(function () {
                  createTransaction((new XMLSerializer()).serializeToString(data), requestType, token);
               }, 1500);
            } else {
               $('#result-response').val(JSON.stringify(data));
               setTimeout(function () {
                  createTransaction(JSON.stringify(data), requestType, token);
               }, 1500);
            }
         }
      });
   });
});