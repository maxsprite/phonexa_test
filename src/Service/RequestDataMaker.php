<?php

namespace App\Service;

use Psr\Container\ContainerInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class RequestDataMaker
{
    /**
     * @var mixed|\Twig\Environment
     */
    private $twig;

    public function __construct(ContainerInterface $container)
    {
        $this->twig = $container->get('twig');
    }

    /**
     * Generating XML from $data
     *
     * @param array $data
     * @return string
     *
     * @throws LoaderError  When the template cannot be found
     * @throws SyntaxError  When an error occurred during compilation
     * @throws RuntimeError When an error occurred during rendering
     */
    public function xml(array $data) {
        return $this->twig->render('xml/request.xml.twig', [
            'data' => $data
        ]);
    }

    /**
     * Generating json from $data
     *
     * @param array $data
     * @return string
     *
     * @throws LoaderError  When the template cannot be found
     * @throws SyntaxError  When an error occurred during compilation
     * @throws RuntimeError When an error occurred during rendering
     */
    public function json(array $data) {
        return $this->twig->render('json/request.json.twig', [
            'data' => $data
        ]);
    }
}