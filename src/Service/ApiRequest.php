<?php

namespace App\Service;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpClient\CurlHttpClient;

class ApiRequest
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * JWT token for API requests
     * @var string
     */
    private $token = false;

    public function __construct(ContainerInterface $container, string $apiUrl)
    {
        $this->container = $container;
        $this->apiUrl= $apiUrl;
    }

    /**
     * Before making any request we need to auth for
     * retrieve JWT token
     *
     * @param $username
     * @param $password
     * @return mixed
     */
    public function auth($username, $password)
    {
        $apiLoginCheckRouter = $this->container->get('router')->getRouteCollection()
            ->get('api_login_check');

        if ($apiLoginCheckRouter) {
            return $this->curlAuth($apiLoginCheckRouter->getPath(), $username, $password);
        }

        return false;
    }

    /**
     * @param $method
     * @param $path
     * @param $data
     * @param bool $token
     * @return mixed
     */
    public function send($method, $path, $data, $token = false, $type = 'application/json')
    {
        try {
            if ($token === false && $this->token === false) {
                throw new \Exception('Sorry, but you need have valid JWT token');
            }

            return $this->curlSend($method, $path, $data, $token, $type);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function curlAuth($path, $username, $password)
    {
        try {
            $httpClient = new CurlHttpClient();
            $response = $httpClient->request('POST', $this->apiUrl . $path, [
                'headers' => [
                    'Content-Type: application/json'
                ],
                'json' => [
                    'username' => $username,
                    'password' => $password
                ]
            ]);

            return $response->getContent();
        } catch (\Exception $e) {
            return false;
        }
    }

    private function curlSend($method, $path, $data, $token, $type = 'application/json')
    {
        try {
            $httpClient = new CurlHttpClient();
            $response = $httpClient->request($method, $this->apiUrl . $path, [
                'headers' => [
                    'Content-Type: ' . $type
                ],
                'json' => [
                    'username' => $username,
                    'password' => $password
                ]
            ]);

            return $response->getContent();
        } catch (\Exception $e) {
            return false;
        }
    }
}