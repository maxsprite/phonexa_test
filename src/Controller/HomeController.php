<?php

namespace App\Controller;

use App\Service\ApiRequest;
use App\Service\RequestDataMaker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(RequestDataMaker $requestDataMaker, ApiRequest $apiRequest)
    {
        return $this->render('home/index.html.twig');
    }
}
