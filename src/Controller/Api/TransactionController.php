<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransactionController
 * @package App\Controller\Api
 *
 * @Route("/api/transaction", name="api_transaction_")
 */
class TransactionController extends AbstractController
{
    private $lead = [
        'firstName' => 'Vasya',
        'lastName' => 'Pupkin',
        'dateOfBirth' => '1984-07-31',
    ];

    private function checkLead($data)
    {
        if ($data['firstName'] == $this->lead['firstName']
            && $data['lastName'] == $this->lead['lastName']
            && $data['dateOfBirth'] == $this->lead['dateOfBirth']
        ) {
            return true;
        }

        return false;
    }

    /**
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            $type = $request->query->get('type');
            if ($type == 'xml') {
                $data = simplexml_load_string($request->getContent());
                $data = json_encode($data);
                $data = json_decode($data, true);
            } else {
                $data = json_decode($request->getContent(), true);
            }

            if (is_array($data)) {
                if ($this->checkLead($data) === true && $data['salary'] >= 700) {
                    $template = 'api/transaction/sold.'. $type .'.twig';
                } else if ($this->checkLead($data) === true && $data['salary'] < 700) {
                    $template = 'api/transaction/reject.'. $type .'.twig';
                } else {
                    $template = 'api/transaction/error.'. $type .'.twig';
                }
            } else {
                $template = 'api/transaction/error.'. $type .'.twig';
            }

            $contentType = 'application/' . $type;
            $response = new Response($this->renderView($template));
            $response->headers->set('Content-Type', $contentType);

            return $response;
        } catch (\Exception $e) {
            $contentType = 'application/' . $type;
            $template = 'api/transaction/error.'. $type .'.twig';
            $response = new Response($this->renderView($template, [
                'data' => $data
            ]));
            $response->headers->set('Content-Type', $contentType);

            return $response;
        }
    }
}
