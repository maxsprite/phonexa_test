<?php

namespace App\Controller;

use App\Service\ApiRequest;
use App\Service\RequestDataMaker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package App\Controller
 *
 * @Route("/ajax", name="ajax_")
 */
class AjaxController extends AbstractController
{
    /**
     * For cute receive JWT token
     *
     * @param Request $request
     * @param ApiRequest $apiRequest
     *
     * @return JsonResponse
     *
     * @Route("/get-over-here", name="get_over_here")
     */
    public function getOverHere(Request $request, ApiRequest $apiRequest)
    {
        $token = $apiRequest->auth($request->get('username'), $request->get('password'));
        return $this->json($token);
    }

    /**
     * Ajax resource for creating transaction
     *
     * @param Request $request
     * @param RequestDataMaker $requestDataMaker
     *
     * @throws
     *
     * @return Response
     *
     * @Route("/create-transaction", name="create_transaction")
     */
    public function createTransaction(Request $request, RequestDataMaker $requestDataMaker)
    {
        $params = $request->request->all();

        if ($request->query->get('type') == 'xml') {
            $data = $requestDataMaker->xml($params);

            $response = new Response($data);
            $response->headers->set('Content-Type', 'application/xml');
        } else {
            $data = $requestDataMaker->json($params);

            $response = new Response($data);
            $response->headers->set('Content-Type', 'application/json');
        }

        return $response;
    }
}
